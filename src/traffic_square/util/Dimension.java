package traffic_square.util;

/**
 * Class representing 2 dimensions
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class Dimension
{
    private int width, height;

    /**
     * Dimension constructor
     *
     * @param width the width of the dimension
     * @param height the height of the dimension
     */
    public Dimension( int width, int height )
    {
        this.width = width;
        this.height = height;
    }

    /**
     * Get the width of the dimension
     *
     * @return the width of the dimension
     */
    public int getWidth()
    {
        return width;
    }

    /**
     * Set the width of the dimension
     *
     * @param width the width of the dimension
     */
    public void setWidth( int width )
    {
        this.width = width;
    }

    /**
     * Get the height of the dimension
     *
     * @return the height of the dimension
     */
    public int getHeight()
    {
        return height;
    }

    /**
     * Set the width of the dimension
     *
     * @param height the width of the dimension
     */
    public void setHeight( int height )
    {
        this.height = height;
    }

    /**
     * Convert into a generic Java Dimension object
     *
     * @return a generic Java Dimension object representing the same dimension
     */
    public java.awt.Dimension toGeneric()
    {
        return new java.awt.Dimension( width, height );
    }
}
