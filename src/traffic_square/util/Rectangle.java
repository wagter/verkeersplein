package traffic_square.util;

/**
 * Created own Rectangle class because I don't want to have to cast to (int) all the time
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class Rectangle
{
    /**
     * The position of the rectangle
     */
    private Position position;

    /**
     * The dimension of the rectangle
     */
    private Dimension dimension;

    /**
     * Rectangle constructor
     *
     * @param x position
     * @param y position
     * @param width of the rectangle
     * @param height of the rectangle
     */
    public Rectangle( int x, int y, int width, int height )
    {
        this( new Position( x, y ), new Dimension( width, height ) );
    }

    /**
     * Rectangle constructor
     *
     * @param position of the rectangle
     * @param dimension of the rectangle
     */
    public Rectangle( Position position, Dimension dimension )
    {
        this.position = position;
        this.dimension = dimension;
    }

    /**
     * Get the position of the rectangle
     *
     * @return the position of the rectangle
     */
    public Position getPosition()
    {
        return position;
    }

    /**
     * Set the position of the rectangle
     */
    public void setPosition( Position position )
    {
        this.position = position;
    }

    /**
     * Get the dimension of the rectangle
     *
     * @return the dimension of the rectangle
     */
    public Dimension getDimension()
    {
        return dimension;
    }

    /**
     * Set the dimension of the rectangle
     *
     * @param dimension of the rectangle
     */
    public void setDimension( Dimension dimension )
    {
        this.dimension = dimension;
    }

    /**
     * Get position x of the rectangle
     *
     * @return position x of the rectangle
     */
    public int getX()
    {
        return position.getX();
    }

    /**
     * Set position x of the rectangle
     *
     * @param x position x of the rectangle
     */
    public void setX( int x )
    {
        position.setX( x );
    }

    /**
     * Get position y of the rectangle
     *
     * @return position x of the rectangle
     */
    public int getY()
    {
        return position.getY();
    }

    /**
     * Set position y of the rectangle
     *
     * @param y position y of the rectangle
     */
    public void setY( int y )
    {
        position.setY( y );
    }

    /**
     * Get the dimension width of the rectangle
     *
     * @return the dimension width of the rectangle
     */
    public int getWidth()
    {
        return dimension.getWidth();
    }

    /**
     * Set the dimension width of the rectangle
     *
     * @param width the dimension width of the rectangle
     */
    public void setWidth( int width )
    {
        dimension.setWidth( width );
    }

    /**
     * Get the dimension height of the rectangle
     *
     * @return the dimension height of the rectangle
     */
    public int getHeight()
    {
        return dimension.getHeight();
    }

    /**
     * Set the dimension height of the rectangle
     *
     * @param height the dimension height of the rectangle
     */
    public void setHeight( int height )
    {
        dimension.setHeight( height );
    }
}
