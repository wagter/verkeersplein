package traffic_square.util;

/**
 * Created own Position class because I don't want to have to cast to int all the time
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class Position
{
    /**
     * The position x and y
     */
    private int x, y;

    /**
     * Position constructor
     *
     * @param x the position x
     * @param y the position y
     */
    public Position( int x, int y )
    {
        this.x = x;
        this.y = y;
    }

    /**
     * Get the position x
     *
     * @return the position x
     */
    public int getX()
    {
        return x;
    }

    /**
     * Set the position x
     *
     * @param x the position x
     */
    public void setX( int x )
    {
        this.x = x;
    }

    /**
     * Get the position y
     *
     * @return the position y
     */
    public int getY()
    {
        return y;
    }

    /**
     * Set the position y
     *
     * @param y the position y
     */
    public void setY( int y )
    {
        this.y = y;
    }
}
