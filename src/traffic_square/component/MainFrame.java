package traffic_square.component;

import traffic_square.util.Dimension;

import javax.swing.*;

/**
 * The main frame of the application
 */
public class MainFrame extends JFrame
{
    /**
     * MainFrame constructor
     *
     * @param dimension the dimension of the frame
     * @param title the title of the frame
     */
    public MainFrame( Dimension dimension, String title )
    {
        setSize( dimension.getWidth(), dimension.getHeight() );
        setResizable( false );
        setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        setTitle( title );
    }
}
