package traffic_square.component;

import traffic_square.util.Position;
import traffic_square.traffic_light.TrafficLight;

/**
 * Factory class to instantiate new TrafficLightComponent objects
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface TrafficLightComponentFactory
{
    /**
     * Instantiate a new TrafficLightComponent
     *
     * @param light the traffic_light instance to display by the TrafficLightComponent
     * @param componentPosition the position of the TrafficLightComponent
     * @return the newly instantiated TrafficLightComponent object
     */
    TrafficLightComponent create( TrafficLight light, Position componentPosition );
}
