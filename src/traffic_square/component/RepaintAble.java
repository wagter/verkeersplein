package traffic_square.component;

/**
 * Interface to enable repaintability for other classes
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface RepaintAble
{
    /**
     * Repaint the object
     */
    void repaint();
}
