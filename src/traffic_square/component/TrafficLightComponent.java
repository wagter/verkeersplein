package traffic_square.component;

import traffic_square.util.Position;
import traffic_square.traffic_light.TrafficLight;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;

/**
 * A JComponent to display a traffic_light
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class TrafficLightComponent extends JComponent
{
    /**
     * The traffic_light instance to display by the component
     */
    protected TrafficLight light;

    /**
     * The width of the component
     */
    public static final int WIDTH = 24;

    /**
     * The height of the component
     */
    public static final int HEIGHT = 60;

    /**
     * The map to determine the color of the lamps
     */
    HashMap<Integer, HashMap<String, String>> lampColorMap;

    /**
     * The map to determine which state should be which color
     */
    private HashMap<String, Color> stateColorMap;

    /**
     * TrafficLightComponent constructor
     *
     * @param light a traffic_light instance to display by the component
     * @param componentPosition the component position
     */
    public TrafficLightComponent( TrafficLight light, Position componentPosition )
    {
        this( light, componentPosition.getX(), componentPosition.getY() );
    }

    /**
     * TrafficLightComponent constructor
     *
     * @param light a traffic_light instance to display by the component
     * @param x the component position x
     * @param y the component position y
     */
    public TrafficLightComponent( TrafficLight light, int x, int y )
    {
        this.light = light;
        setPreferredSize( new java.awt.Dimension( getLightWidth(), getLightHeight() ) );
        setBounds( x, y, getLightWidth(), getLightHeight() );
    }

    @Override
    public void paintComponent( Graphics g )
    {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
        super.paintComponent( g2d );

        g.setColor( Color.BLACK );
        g.fillRoundRect( 0, 0, getLightWidth(), getLightHeight(), 20, 20 );

        for ( int i = 0; i < 3; i++ ) {
            drawLamp( g2d, i );
        }
    }

    /**
     * Draw a lamp by its index
     *
     * @param g2d the Graphics2D object
     * @param index the index of the lamp
     */
    void drawLamp( Graphics2D g2d, int index )
    {
        g2d.setColor( getLampColor( index ) );
        Point position = getLampPosition( index );
        g2d.fillOval( (int) position.getX(), (int) position.getY(), getLampSize(), getLampSize() );
    }

    /**
     * Get the traffic_square width
     *
     * @return the traffic_square width
     */
    public int getLightWidth()
    {
        if ( light.isOrientation( TrafficLight.ORIENTATION_NORTH ) || light.isOrientation( TrafficLight.ORIENTATION_SOUTH ) ) {
            return WIDTH;
        }

        return HEIGHT;
    }

    /**
     * The traffic_square height
     *
     * @return the traffic_square height
     */
    public int getLightHeight()
    {
        if ( light.isOrientation( TrafficLight.ORIENTATION_NORTH ) || light.isOrientation( TrafficLight.ORIENTATION_SOUTH ) ) {
            return HEIGHT;
        }

        return WIDTH;
    }

    /**
     * Get the lamp size
     *
     * @return the lamp size
     */
    protected int getLampSize()
    {
        return WIDTH / 2;
    }

    /**
     * Get the position a lamp should have by its index
     *
     * @param lampIndex the index of the lamp
     * @return the position of the lamp
     */
    protected Point getLampPosition( int lampIndex )
    {
        int x = 6;
        x += ( light.isOrientation( TrafficLight.ORIENTATION_NORTH ) || light.isOrientation( TrafficLight.ORIENTATION_SOUTH ) )
            ? 0
            : lampIndex * getLightHeight() - lampIndex * 6
        ;

        int y = 6;
        y += ( light.isOrientation( TrafficLight.ORIENTATION_NORTH ) || light.isOrientation( TrafficLight.ORIENTATION_SOUTH ) )
            ? lampIndex * getLightWidth() - lampIndex * 6
            : 0
        ;

        return new Point( x, y );
    }

    /**
     * Get the color of a lamp by its index and state
     *
     * @param lampIndex the index of the lamp
     * @return the color of the lamp
     */
    private Color getLampColor( int lampIndex )
    {
        if ( getLampColorMap().containsKey( lampIndex ) ) {

            HashMap<String, String> map = getLampColorMap().get( lampIndex );

            if ( light.isState( map.get( light.getOrientation() ) ) ) {
                return getStateColorMap().get( light.getState() );
            }
        }

        return getStateColorMap().get( TrafficLight.STATE_OFF );
    }

    /**
     * Get the map to depend which color which lamp should have
     *
     * @return the map to depend which color which lamp should have
     */
    protected HashMap<Integer, HashMap<String, String>> getLampColorMap()
    {
        if ( lampColorMap != null ) {
            return lampColorMap;
        }

        lampColorMap = new HashMap<Integer, HashMap<String, String>>() {{

            put( 0, new HashMap<String, String>(  ) {{
                put( TrafficLight.ORIENTATION_EAST, TrafficLight.STATE_GREEN );
                put( TrafficLight.ORIENTATION_SOUTH, TrafficLight.STATE_GREEN );
                put( TrafficLight.ORIENTATION_WEST, TrafficLight.STATE_RED );
                put( TrafficLight.ORIENTATION_NORTH, TrafficLight.STATE_RED );
            }} );

            put( 1, new HashMap<String, String>(  ) {{
                put( TrafficLight.ORIENTATION_EAST, TrafficLight.STATE_YELLOW );
                put( TrafficLight.ORIENTATION_SOUTH, TrafficLight.STATE_YELLOW );
                put( TrafficLight.ORIENTATION_WEST, TrafficLight.STATE_YELLOW );
                put( TrafficLight.ORIENTATION_NORTH, TrafficLight.STATE_YELLOW );
            }} );

            put( 2, new HashMap<String, String>(  ) {{
                put( TrafficLight.ORIENTATION_EAST, TrafficLight.STATE_RED );
                put( TrafficLight.ORIENTATION_SOUTH, TrafficLight.STATE_RED );
                put( TrafficLight.ORIENTATION_WEST, TrafficLight.STATE_GREEN );
                put( TrafficLight.ORIENTATION_NORTH, TrafficLight.STATE_GREEN );
            }} );

        }};

        return getLampColorMap();
    }

    private HashMap<String, Color> getStateColorMap()
    {
        if ( stateColorMap != null ) {
            return stateColorMap;
        }

        stateColorMap = new HashMap<String, Color>() {{
            put( TrafficLight.STATE_GREEN, new Color( 118,255,3 ) );
            put( TrafficLight.STATE_YELLOW, new Color( 249, 168, 37 ) );
            put( TrafficLight.STATE_RED, new Color( 211,47,47 ) );
            put( TrafficLight.STATE_OFF, new Color( 33, 33, 33 ) );
        }};

        return getStateColorMap();
    }

}
