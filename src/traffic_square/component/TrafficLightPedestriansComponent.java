package traffic_square.component;

import traffic_square.traffic_light.TrafficLight;
import traffic_square.util.Position;

import java.awt.*;
import java.util.HashMap;

/**
 * component to display a traffic_light instance with TYPE_PEDESTRIANS
 */
public class TrafficLightPedestriansComponent extends TrafficLightComponent
{
    public static final int WIDTH = 16;

    public static final int HEIGHT = 30;

    TrafficLightPedestriansComponent( TrafficLight light, Position componentPosition )
    {
        super( light, componentPosition );
    }

    @Override
    public void paint( Graphics g )
    {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );
        super.paint( g2d );

        g.setColor( Color.BLACK );
        g.fillRoundRect( 0, 0, getLightWidth(), getLightHeight(), 12, 12 );

        for ( int i = 0; i < 2; i++ ) {
            drawLamp( g2d, i );
        }
    }

    @Override
    protected HashMap<Integer, HashMap<String, String>> getLampColorMap()
    {
        if ( lampColorMap != null ) {
            return lampColorMap;
        }

        lampColorMap = new HashMap<>();

        HashMap<String, String> lampMap = new HashMap<>();
        lampMap.put( TrafficLight.ORIENTATION_EAST, TrafficLight.STATE_GREEN );
        lampMap.put( TrafficLight.ORIENTATION_SOUTH, TrafficLight.STATE_GREEN );
        lampMap.put( TrafficLight.ORIENTATION_WEST, TrafficLight.STATE_RED );
        lampMap.put( TrafficLight.ORIENTATION_NORTH, TrafficLight.STATE_RED );

        lampColorMap.put( 0, lampMap );

        HashMap<String, String> lamp2Map = new HashMap<>();
        lamp2Map.put( TrafficLight.ORIENTATION_EAST, TrafficLight.STATE_RED );
        lamp2Map.put( TrafficLight.ORIENTATION_SOUTH, TrafficLight.STATE_RED );
        lamp2Map.put( TrafficLight.ORIENTATION_WEST, TrafficLight.STATE_GREEN );
        lamp2Map.put( TrafficLight.ORIENTATION_NORTH, TrafficLight.STATE_GREEN );

        lampColorMap.put( 1, lamp2Map );

        return getLampColorMap();
    }

    @Override
    protected Point getLampPosition( int lampIndex )
    {
        int x = 3;
        x += ( light.isOrientation( TrafficLight.ORIENTATION_NORTH ) || light.isOrientation( TrafficLight.ORIENTATION_SOUTH ) )
            ? 0
            : lampIndex * getLightHeight() - lampIndex * 3
        ;

        int y = 3;
        y += ( light.isOrientation( TrafficLight.ORIENTATION_NORTH ) || light.isOrientation( TrafficLight.ORIENTATION_SOUTH ) )
            ? lampIndex * getLightWidth() - lampIndex * 3
            : 0
        ;

        return new Point( x, y );
    }

    @Override
    protected int getLampSize()
    {
        return 10;
    }

    @Override
    public int getLightWidth()
    {
        if ( light.isOrientation( TrafficLight.ORIENTATION_NORTH ) || light.isOrientation( TrafficLight.ORIENTATION_SOUTH ) ) {
            return WIDTH;
        }

        return HEIGHT;
    }

    @Override
    public int getLightHeight()
    {
        if ( light.isOrientation( TrafficLight.ORIENTATION_NORTH ) || light.isOrientation( TrafficLight.ORIENTATION_SOUTH ) ) {
            return HEIGHT;
        }

        return WIDTH;
    }
}
