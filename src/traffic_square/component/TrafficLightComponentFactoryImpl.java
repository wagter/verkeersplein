package traffic_square.component;

import traffic_square.util.Position;
import traffic_square.traffic_light.TrafficLight;

/**
 * TrafficLightComponent implementation
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class TrafficLightComponentFactoryImpl implements TrafficLightComponentFactory
{
    @Override
    public TrafficLightComponent create( TrafficLight light, Position componentPosition )
    {
        if ( light.isType( TrafficLight.TYPE_PEDESTRIANS )) {
            return new TrafficLightPedestriansComponent( light, componentPosition );
        }

        return new TrafficLightComponent( light, componentPosition );
    }
}
