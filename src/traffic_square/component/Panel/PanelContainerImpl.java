package traffic_square.component.Panel;

import traffic_square.component.Background.*;
import traffic_square.component.TrafficLightComponent;
import traffic_square.config.ConfigParameterBag;
import traffic_square.config.exception.InvalidConfigException;
import traffic_square.controller.command.CommandMessageList;
import traffic_square.util.Dimension;
import traffic_square.util.Position;

import java.awt.*;
import java.util.List;

public class PanelContainerImpl implements PanelContainer
{
    private MainPanel mainPanel;

    private ControlPanel controlPanel;

    private CommandMessagePanel commandMessagePanel;

    private TrafficSquarePanel trafficSquarePanel;

    private BasePanel buttonPanel;

    private Dimension backgroundDimension;

    private Color backgroundColor;

    private int controlPanelWidth;

    /**
     * PanelContainerImpl constructor
     *
     * @param config the config parameter bag
     *
     * @throws InvalidConfigException if the needed parameters are not present
     */
    public PanelContainerImpl( ConfigParameterBag config ) throws InvalidConfigException
    {

        backgroundDimension = new Dimension(
            config.getInteger( "background_width" ),
            config.getInteger( "background_height" )
        );

        backgroundColor = config.getColor( "background" );
        controlPanelWidth = config.getInteger( "control_panel_width" );
    }

    @Override
    public MainPanel getMainPanel()
    {
        if ( mainPanel != null ) {
            return mainPanel;
        }

        mainPanel = new MainPanel( backgroundDimension );

        return getMainPanel();
    }

    @Override
    public ControlPanel getControlPanel()
    {
        if ( controlPanel != null ) {
            return controlPanel;
        }

        controlPanel = new ControlPanel(
            new Dimension( controlPanelWidth, backgroundDimension.getHeight() ),
            backgroundColor
        );

        return getControlPanel();
    }

    @Override
    public CommandMessagePanel getCommandMessagePanel( CommandMessageList messageList )
    {
        if ( commandMessagePanel != null ) {
            return commandMessagePanel;
        }

        commandMessagePanel = new CommandMessagePanel( messageList );

        return getCommandMessagePanel( messageList );
    }

    @Override
    public TrafficSquarePanel getTrafficSquarePanel( List<TrafficLightComponent> components )
    {
        if ( trafficSquarePanel != null ) {
            return trafficSquarePanel;
        }

        trafficSquarePanel = new TrafficSquarePanel(
            backgroundDimension,
            getBackground(),
            components
        );

        return getTrafficSquarePanel( components );
    }

    @Override
    public BasePanel getButtonPanel()
    {
        if ( buttonPanel != null ) {
            return buttonPanel;
        }

        buttonPanel = new BasePanel() {};

        buttonPanel.setPreferredSize( new java.awt.Dimension( controlPanelWidth, 50 ) );
        buttonPanel.setBackground( backgroundColor );

        return getButtonPanel();
    }

    /**
     * Get the traffic square panel background
     *
     * @return the traffic square panel background
     */
    private BackgroundComponentCollection getBackground()
    {
        return new TrafficSquarePanelBackground()
        {{

            int centerX = backgroundDimension.getWidth() / 2;
            int centerY = backgroundDimension.getHeight() / 2;

            // GRASS
            add( new GrassComponent(
                new traffic_square.util.Rectangle( new Position( 0, 0 ), backgroundDimension )
            ) );

            // SIDEWALKS
            add( new SidewalkComponent(
                new traffic_square.util.Rectangle(
                    0,
                    centerY - RoadComponent.WIDTH / 2 - RoadComponent.BERM_WIDTH - SidewalkComponent.WIDTH,
                    backgroundDimension.getWidth(),
                    SidewalkComponent.WIDTH
                )
            ) );
            add( new SidewalkComponent(
                new traffic_square.util.Rectangle(
                    0,
                    centerY + RoadComponent.WIDTH / 2 + RoadComponent.BERM_WIDTH,
                    backgroundDimension.getWidth(),
                    SidewalkComponent.WIDTH
                ) )
            );
            add( new SidewalkComponent(
                new traffic_square.util.Rectangle(
                    centerX + RoadComponent.WIDTH / 2 + RoadComponent.BERM_WIDTH,
                    0,
                    SidewalkComponent.WIDTH,
                    backgroundDimension.getHeight()
                )
            ) );
            add( new SidewalkComponent(
                new traffic_square.util.Rectangle(
                    centerX - RoadComponent.WIDTH / 2 - RoadComponent.BERM_WIDTH - SidewalkComponent.WIDTH,
                    0,
                    SidewalkComponent.WIDTH,
                    backgroundDimension.getHeight()
                )
            ) );

            // ROADS
            add( new RoadComponent(
                new Position( centerX - RoadComponent.WIDTH / 2, 0 ),
                backgroundDimension.getHeight(),
                true
            ) );
            add( new RoadComponent(
                new Position( 0, centerY - RoadComponent.WIDTH / 2 ),
                backgroundDimension.getWidth(),
                false
            ) );

            // ZEBRA CROSSINGS
            add( new ZebraCrossingComponent(
                new Position(
                    centerX - RoadComponent.WIDTH / 2,
                    centerY - RoadComponent.WIDTH / 2 - RoadComponent.BERM_WIDTH - SidewalkComponent.WIDTH
                ),
                false
            ) );
            add( new ZebraCrossingComponent(
                new Position(
                    centerX - RoadComponent.WIDTH / 2,
                    centerY + RoadComponent.WIDTH / 2 + RoadComponent.BERM_WIDTH
                ),
                false
            ) );
            add( new ZebraCrossingComponent(
                new Position(
                    centerX - RoadComponent.WIDTH / 2 - RoadComponent.BERM_WIDTH - SidewalkComponent.WIDTH,
                    centerY - RoadComponent.WIDTH / 2
                ),
                true
            ) );
            add( new ZebraCrossingComponent(
                new Position(
                    centerX + RoadComponent.WIDTH / 2 + RoadComponent.BERM_WIDTH,
                    centerY - RoadComponent.WIDTH / 2
                ),
                true
            ) );
        }};
    }
}
