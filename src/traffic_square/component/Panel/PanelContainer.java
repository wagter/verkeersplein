package traffic_square.component.Panel;

import traffic_square.component.TrafficLightComponent;
import traffic_square.controller.command.CommandMessageList;

import java.util.List;

/**
 * Container to instantiate and keep panels
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface PanelContainer
{
    /**
     * Get the main panel
     *
     * @return the main panel
     */
    MainPanel getMainPanel();

    /**
     * Get the control panel
     *
     * @return the control panel
     */
    ControlPanel getControlPanel();

    /**
     * Get the command message panel
     *
     * @param messageList the list containing the command messages to display
     *
     * @return the command message panel
     */
    CommandMessagePanel getCommandMessagePanel( CommandMessageList messageList );

    /**
     * Get the traffic square panel
     *
     * @param components the TrafficLightComponent instances to display in the panel
     *
     * @return the traffic square panel
     */
    TrafficSquarePanel getTrafficSquarePanel( List<TrafficLightComponent> components );

    /**
     * Get the button panel
     *
     * @return the button panel
     */
    BasePanel getButtonPanel();
}
