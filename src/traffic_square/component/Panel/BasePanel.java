package traffic_square.component.Panel;

import traffic_square.component.RepaintAble;

import javax.swing.*;

/**
 * Base class to extend for panels
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public abstract class BasePanel extends JPanel implements RepaintAble
{
}
