package traffic_square.component.Panel;

import traffic_square.util.Dimension;

import java.awt.*;

/**
 * The main panel of the application
 *
 * @author Joris Wagter
 */
public class MainPanel extends BasePanel
{
    /**
     * MainPanel constructor
     *
     * @param dimension the dimension of the main panel
     */
    public MainPanel( Dimension dimension )
    {
        setPreferredSize( new java.awt.Dimension(
            dimension.getWidth() + 300,
            dimension.getHeight()
        ) );

        setLayout( new BorderLayout() );
    }
}
