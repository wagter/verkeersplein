package traffic_square.component.Panel;

import traffic_square.util.Dimension;

import java.awt.*;

/**
 * Panel to contain the controls of the application
 *
 * @author Joris Wagter
 */
public class ControlPanel extends BasePanel
{
    public ControlPanel( Dimension dimension, Color bgColor)
    {
        setBackground( bgColor );
        setPreferredSize( dimension.toGeneric() );
        setLayout( new BorderLayout(  ) );
    }
}
