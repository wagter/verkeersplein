package traffic_square.component.Panel;

import traffic_square.component.TrafficLightComponent;
import traffic_square.component.Background.BackgroundComponentCollection;
import traffic_square.util.Dimension;

import java.awt.*;
import java.util.List;

/**
 * A JPanel to display the traffic square
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class TrafficSquarePanel extends BasePanel
{
    private BackgroundComponentCollection background;

    /**
     * TrafficSquarePanel constructor
     *
     * @param dimension       the dimension of the panel
     * @param background      the background of the panel
     * @param lightComponents the TrafficLightComponent instances
     */
    public TrafficSquarePanel( Dimension dimension, BackgroundComponentCollection background, List<TrafficLightComponent> lightComponents )
    {
        setBounds( 0, 0, dimension.getWidth(), dimension.getHeight() );
        setPreferredSize( dimension.toGeneric() );
        setLayout( null );

        for ( TrafficLightComponent lightComponent : lightComponents ) {
            add( lightComponent );
        }

        this.background = background;
    }

    @Override
    protected void paintComponent( Graphics g )
    {
        super.paintComponent( g );
        background.draw( (Graphics2D) g );

    }
}
