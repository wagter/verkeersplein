package traffic_square.component.Panel;

import traffic_square.controller.command.CommandMessageList;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.text.*;
import java.awt.*;

/**
 * A panel to display the messages from a CommandMessageList instance
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class CommandMessagePanel extends BasePanel
{
    /**
     * The CommandMessageList instance to display the messages from
     */
    private CommandMessageList messageList;

    /**
     * The JTextPane instance to put the text in
     */
    private JTextPane textPane;

    /**
     * The JScrollPane instance to provide scrolling in the message list
     */
    private JScrollPane scrollPane;

    /**
     * The JLabel instance to display above the message list
     */
    private JLabel label;

    /**
     * CommandMessagePanel constructor
     *
     * @param messageList the CommandMessageList instance to display the messages from
     */
    public CommandMessagePanel( CommandMessageList messageList )
    {
        this.messageList = messageList;

        setLayout( new BorderLayout() );
        setBackground( new Color( 33, 33, 33 ) );

        JPanel labelPanel = new JPanel();
        labelPanel.setBorder( new EmptyBorder( new Insets( 10, 10, 10, 10 ) )  );
        labelPanel.setBackground( new Color( 33, 33, 33 ) );
        label = new JLabel( "Queue" );
        label.setForeground( new Color( 255, 255, 255 ) );

        labelPanel.add( label );

        add( labelPanel, BorderLayout.NORTH );
        add( getScrollPane(), BorderLayout.CENTER );

    }

    @Override
    public void paintComponent( Graphics g )
    {
        super.paintComponent( g );
        String listContent = "";

        for ( String s : messageList ) {
            listContent = listContent.concat( s + "\n" );
        }

        getTextPane().setText( listContent );
        label.setText( messageList.size() > 0 ? "Queue" : "Queue (empty)" );
    }

    /**
     * Get the JTextPane instance to put the text in. Instantiate first if needed
     *
     * @return the JTextPane instance to put the text in
     */
    private JTextPane getTextPane()
    {
        if ( textPane != null ) {
            return textPane;
        }

        textPane = new JTextPane();
        textPane.setBorder( new EmptyBorder( new Insets( 10, 10, 10, 10 ) ) );
        textPane.setEditable( false );
        textPane.setBackground( new Color( 33, 33, 33 ) );

        StyleContext sc = StyleContext.getDefaultStyleContext();
        AttributeSet asset = sc.addAttribute( SimpleAttributeSet.EMPTY, StyleConstants.Foreground, new Color( 255, 255, 255 ) );
        asset = sc.addAttribute( asset, StyleConstants.FontFamily, "Lucida Console" );
        asset = sc.addAttribute( asset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED );
        textPane.setCharacterAttributes( asset, false );

        DefaultCaret caret = (DefaultCaret) textPane.getCaret();
        caret.setUpdatePolicy(DefaultCaret.NEVER_UPDATE);

        return getTextPane();
    }

    /**
     * Get the JScrollPane instance to provide scrolling in the message list. Instantiate first if needed
     *
     * @return the JScrollPane instance to provide scrolling in the message list
     */
    private JScrollPane getScrollPane()
    {
        if ( scrollPane != null ) {
            return scrollPane;
        }

        scrollPane = new JScrollPane( getTextPane(), ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER );
        scrollPane.setPreferredSize( new java.awt.Dimension( 300, 600 ) );
        scrollPane.setViewportBorder( BorderFactory.createEmptyBorder( 0, 0, 0, 0 ) );
        scrollPane.setBorder( null );

        return getScrollPane();
    }
}
