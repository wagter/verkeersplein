package traffic_square.component.Background;

import java.awt.*;
import java.util.ArrayList;

/**
 * The background to draw on a TrafficSquarePanel instance
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class TrafficSquarePanelBackground extends ArrayList<BackgroundComponent> implements BackgroundComponentCollection
{
    /**
     * Draw the BackgroundComponent instances
     *
     * @param g2d the Graphics2D instance of the panel
     */
    public void draw( Graphics2D g2d )
    {
        for ( BackgroundComponent component : this ) {
            component.draw( g2d );
        }
    }
}
