package traffic_square.component.Background;

import traffic_square.util.Position;

import java.awt.*;

/**
 * Background component displaying a zebra crossing
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class ZebraCrossingComponent implements BackgroundComponent
{
    public static final int STRIPE_WIDTH = 15;

    public static final int STRIPE_SPACING = 10;

    private Color stripeColor = new Color( 255, 255, 255 );

    private Position position;

    private boolean vertical;

    /**
     * ZebraCrossingComponent constructor
     *
     * @param position the position of the zebra crossing
     * @param vertical true if the zebra crossing is vertical
     */
    public ZebraCrossingComponent( Position position, boolean vertical )
    {
        this.position = position;
        this.vertical = vertical;
    }

    @Override
    public void draw( Graphics2D g2d )
    {
        g2d.setColor( stripeColor );

        for ( int i = 0; i < 7; i++ ) {

            int x = vertical ? position.getX() : position.getX() + ( STRIPE_WIDTH + STRIPE_SPACING ) * i ;
            int y = vertical ? position.getY() + ( STRIPE_WIDTH + STRIPE_SPACING ) * i : position.getY();
            int w = vertical ? SidewalkComponent.WIDTH : STRIPE_WIDTH;
            int h = vertical ? STRIPE_WIDTH : SidewalkComponent.WIDTH;

            g2d.fillRect( x, y, w, h );
        }
    }
}
