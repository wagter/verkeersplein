package traffic_square.component.Background;

import traffic_square.util.Rectangle;

import java.awt.*;

/**
 * Background component displaying a sidewalk
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class SidewalkComponent implements BackgroundComponent
{
    public static final int WIDTH = 63;

    private final Color jointColor = new Color( 117, 117, 117 );

    private final Color tileColor = new Color( 158, 158, 158 );

    private Rectangle rectangle;

    /**
     * SidewalkComponent constructor
     *
     * @param rectangle the position and dimensions of the sidewalk
     */
    public SidewalkComponent( Rectangle rectangle )
    {
        this.rectangle = rectangle;
    }

    @Override
    public void draw( Graphics2D g2d )
    {
        g2d.setColor( jointColor );

        g2d.fillRect(
            rectangle.getX(),
            rectangle.getY(),
            rectangle.getWidth(),
            rectangle.getHeight()
        );

        drawTiles( g2d, rectangle );
    }

    /**
     * Draw tiles in a given rectangle
     *
     * @param g2d the Graphics2D instance of the panel
     * @param rectangle the rectangle to draw the tiles in
     */
    private void drawTiles( Graphics2D g2d, Rectangle rectangle )
    {
        boolean vertical = rectangle.getHeight() > rectangle.getWidth();

        int jointSize = 1;
        int tileSize = 15;

        int tileIndex = 0;
        int rowIndex = 0;

        g2d.setColor( tileColor );

        int limitRow = vertical ? rectangle.getWidth() : rectangle.getHeight();
        int limitTile = vertical ? rectangle.getHeight() : rectangle.getWidth();

        while ( ( rowIndex + 1 ) * ( tileSize + jointSize ) - jointSize <= limitRow ) {

            int offset = rowIndex % 2 * ( tileSize / 2 ) - jointSize;
            int offsetX = vertical ? 0 : offset;
            int offsetY = vertical ? offset : 0;

            while ( tileIndex * tileSize + jointSize <= limitTile ) {

                int tileY = vertical
                    ? rectangle.getY() + ( tileSize + jointSize ) * tileIndex - offsetY
                    : rectangle.getY() + ( tileSize + jointSize ) * rowIndex - offsetY;

                int tileX = vertical
                    ? rectangle.getX() + rowIndex * ( tileSize + jointSize ) - offsetX
                    : rectangle.getX() + tileIndex * ( tileSize + jointSize ) - offsetX;

                g2d.fillRect( tileX, tileY, tileSize, tileSize );
                tileIndex += 1;

            }

            tileIndex = 0;
            rowIndex++;
        }

    }
}
