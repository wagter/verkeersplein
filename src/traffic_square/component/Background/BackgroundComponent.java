package traffic_square.component.Background;

import java.awt.*;

/**
 * A component of a drawable background
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface BackgroundComponent
{
    /**
     * Draw the component
     *
     * @param g2d the Graphics2D instance of the parent panel
     */
    void draw( Graphics2D g2d );
}
