package traffic_square.component.Background;

import traffic_square.util.Rectangle;

import java.awt.*;

/**
 * Background component displaying grass
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class GrassComponent implements BackgroundComponent
{
    private Rectangle rectangle;

    private Color color = new Color( 27, 94, 32 );

    /**
     * GrassComponent constructor
     *
     * @param rectangle the position and dimension of the component
     */
    public GrassComponent( Rectangle rectangle )
    {
        this.rectangle = rectangle;
    }

    @Override
    public void draw( Graphics2D g2d )
    {
        g2d.setColor( color );

        g2d.fillRect(
            rectangle.getX(),
            rectangle.getY(),
            rectangle.getWidth(),
            rectangle.getHeight()
        );
    }
}
