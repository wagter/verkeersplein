package traffic_square.component.Background;

import java.util.Collection;

/**
 * A collection of BackgroundComponent instances
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface BackgroundComponentCollection extends BackgroundComponent, Collection<BackgroundComponent>
{
}
