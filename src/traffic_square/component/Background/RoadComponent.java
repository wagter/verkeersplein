package traffic_square.component.Background;

import traffic_square.util.Position;

import java.awt.*;

/**
 * Background component displaying a road
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class RoadComponent implements BackgroundComponent
{
    public static final int WIDTH = 165;

    public static final int BERM_WIDTH = 30;

    private final Color roadColor = new Color( 66, 66, 66 );

    private final Color lineColor = new Color( 255, 255, 255 );

    private Position position;

    private int length;

    private boolean vertical;

    /**
     * RoadComponent constructor
     *
     * @param position the position of the road
     * @param length the length of the road
     * @param vertical true if the road is vertical
     */
    public RoadComponent( Position position, int length, boolean vertical )
    {
        this.position = position;
        this.length = length;
        this.vertical = vertical;
    }

    @Override
    public void draw( Graphics2D g2d )
    {
        g2d.setColor( roadColor );

        g2d.fillRect(
            position.getX(),
            position.getY(),
            vertical ? WIDTH : length,
            vertical ? length : WIDTH
        );

        drawLines( g2d );
        drawClearance( g2d );
    }

    /**
     * Draw the center lines of the road
     *
     * @param g2d the Graphics2D instance of the parent panel
     */
    private void drawLines( Graphics2D g2d )
    {
        int lineLength = 20;
        int lineSpacing = 12;

        g2d.setColor( lineColor );

        int lineIndex = 0;

        while ( lineIndex * lineSpacing + lineIndex * lineLength + lineLength < length ) {

            int lineWidth = 2;
            int x = vertical
                ? position.getX() + ( WIDTH / 2 ) - ( lineWidth / 2 )
                : lineIndex * lineSpacing + lineIndex * lineLength;
            int y = vertical
                ? lineIndex * lineSpacing + lineIndex * lineLength
                : position.getY() + ( WIDTH / 2 ) - ( lineWidth / 2 );
            int w = vertical ? lineWidth : lineLength;
            int h = vertical ? lineLength : lineWidth;

            g2d.fillRect( x, y, w, h );
            lineIndex++;
        }
    }

    /**
     * Clear the lines on the middle of the road, where the roads intersect
     *
     * @param g2d the Graphics2D instance of the parent panel
     */
    private void drawClearance( Graphics2D g2d )
    {
        g2d.setColor( roadColor );

        int x = vertical ? position.getX() : ( length / 2 ) - ( WIDTH / 2 ) - BERM_WIDTH;
        int y = vertical ? ( length / 2 ) - ( WIDTH / 2 ) - BERM_WIDTH : position.getY();
        int w = vertical ? WIDTH : WIDTH + BERM_WIDTH * 2;
        int h = vertical ? WIDTH + BERM_WIDTH * 2 : WIDTH;

        g2d.fillRect( x, y, w, h );
    }
}
