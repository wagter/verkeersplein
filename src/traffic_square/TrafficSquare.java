package traffic_square;

import traffic_square.listener.LoopActionListener;
import traffic_square.listener.StepActionListener;
import traffic_square.listener.ToggleActionListener;
import traffic_square.builder.TrafficLightBuilder;
import traffic_square.builder.TrafficLightBuilderImpl;
import traffic_square.component.*;
import traffic_square.config.ConfigParameterBag;
import traffic_square.config.exception.InvalidConfigException;
import traffic_square.controller.command.CommandMessageList;
import traffic_square.controller.command.CommandMessageListImpl;
import traffic_square.controller.command.CommandQueue;
import traffic_square.controller.command.CommandQueueImpl;
import traffic_square.controller.TrafficSquareController;
import traffic_square.component.Panel.*;
import traffic_square.component.Background.*;
import traffic_square.traffic_light.TrafficLight;
import traffic_square.traffic_light.TrafficLightFactoryImpl;
import traffic_square.util.Dimension;
import traffic_square.util.Position;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * The main class of the Application application
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class TrafficSquare implements Application
{
    /**
     * Parameter bag containing values for configuring the application
     */
    private ConfigParameterBag config;

    /**
     * builder object to build traffic_light instances and Components
     */
    private TrafficLightBuilder builder;

    /**
     * The dimension of the background
     */
    private Dimension backgroundDimension;

    /**
     * Container to instantiate and keep panel objects
     */
    private PanelContainer panelContainer;

    /**
     * TrafficSquare constructor
     *
     * @param config a parameter bag containing values for configuring the application
     * @throws InvalidConfigException if the config parameter bag does not contain valid config
     */
    public TrafficSquare( ConfigParameterBag config ) throws InvalidConfigException
    {
        this.config = config;

        backgroundDimension = new Dimension(
            config.getInteger( "background_width" ),
            config.getInteger( "background_height" )
        );

        panelContainer = new PanelContainerImpl( config );
    }

    @Override
    public void init() throws InvalidConfigException
    {
        CommandMessageList messageList = new CommandMessageListImpl();
        CommandQueue commandQueue = new CommandQueueImpl( messageList );

        TrafficSquarePanel trafficSquarePanel = panelContainer.getTrafficSquarePanel( getBuilder().getComponents() );
        CommandMessagePanel commandMessagePanel = panelContainer.getCommandMessagePanel( messageList );

        List<RepaintAble> repaintAbles = new ArrayList<RepaintAble>()
        {{
            add( trafficSquarePanel );
            add( commandMessagePanel );
        }};

        TrafficSquareController squareController = new TrafficSquareController(
            commandQueue,
            getBuilder().getLights(),
            repaintAbles
        );

        JButton toggleBtn = new JButton( "ON / OFF" );
        JButton loopBtn = new JButton( "LOOP" );
        JButton stepBtn = new JButton( "NEXT" );

        toggleBtn.addActionListener( new ToggleActionListener( squareController ) );
        loopBtn.addActionListener( new LoopActionListener( squareController ) );
        stepBtn.addActionListener( new StepActionListener( squareController ) );

        panelContainer.getButtonPanel().add( toggleBtn );
        panelContainer.getButtonPanel().add( loopBtn );
        panelContainer.getButtonPanel().add( stepBtn );

        panelContainer.getControlPanel().add( panelContainer.getButtonPanel(), BorderLayout.NORTH );
        panelContainer.getControlPanel().add( commandMessagePanel, BorderLayout.CENTER );

        panelContainer.getMainPanel().add( panelContainer.getControlPanel(), BorderLayout.EAST );
        panelContainer.getMainPanel().add( trafficSquarePanel, BorderLayout.CENTER );

        MainFrame frame = new MainFrame(
            new Dimension(
                backgroundDimension.getWidth() + config.getInteger( "control_panel_width" ),
                backgroundDimension.getHeight()
            ),
            "Verkeersplein"
        );
        frame.setContentPane( panelContainer.getMainPanel() );
        frame.setVisible( true );
    }

    /**
     * Get the TrafficLightBuilder instance. If needed, instantiate first.
     *
     * @return the TrafficLightBuilder instance
     */
    private TrafficLightBuilder getBuilder()
    {
        if ( builder != null ) {
            return builder;
        }

        int centerX = backgroundDimension.getWidth() / 2;
        int centerY = backgroundDimension.getHeight() / 2;

        int trafficLightRoadSpacing = 6;

        builder = new TrafficLightBuilderImpl( new TrafficLightFactoryImpl(), new TrafficLightComponentFactoryImpl() )
        {{
            // VEHICLES
            add( TrafficLight.TYPE_VEHICLES,
                 TrafficLight.ORIENTATION_EAST,
                 TrafficLight.STATE_OFF,
                 new Position(
                     centerX - ( RoadComponent.WIDTH / 2 ) - RoadComponent.BERM_WIDTH - SidewalkComponent.WIDTH - TrafficLightComponent.HEIGHT - trafficLightRoadSpacing,
                     centerY + ( RoadComponent.WIDTH / 2 ) - ( RoadComponent.WIDTH / 4 ) - ( TrafficLightComponent.WIDTH / 2 )
                 )
            );
            add( TrafficLight.TYPE_VEHICLES,
                 TrafficLight.ORIENTATION_SOUTH,
                 TrafficLight.STATE_OFF,
                 new Position(
                     centerX - ( RoadComponent.WIDTH / 2 ) + ( RoadComponent.WIDTH / 4 ) - ( TrafficLightComponent.WIDTH / 2 ),
                     centerY - ( RoadComponent.WIDTH / 2 ) - RoadComponent.BERM_WIDTH - SidewalkComponent.WIDTH - TrafficLightComponent.HEIGHT - trafficLightRoadSpacing
                 )
            );
            add( TrafficLight.TYPE_VEHICLES,
                 TrafficLight.ORIENTATION_WEST,
                 TrafficLight.STATE_OFF,
                 new Position(
                     centerX + ( RoadComponent.WIDTH / 2 ) + RoadComponent.BERM_WIDTH + TrafficLightComponent.HEIGHT + trafficLightRoadSpacing,
                     centerY - ( RoadComponent.WIDTH / 2 ) + ( RoadComponent.WIDTH / 4 ) - ( TrafficLightComponent.WIDTH / 2 )
                 )
            );
            add( TrafficLight.TYPE_VEHICLES,
                 TrafficLight.ORIENTATION_NORTH,
                 TrafficLight.STATE_OFF,
                 new Position(
                     centerX + ( RoadComponent.WIDTH / 4 ) - ( TrafficLightComponent.WIDTH / 2 ),
                     centerY + ( RoadComponent.WIDTH / 2 ) + RoadComponent.BERM_WIDTH + SidewalkComponent.WIDTH + trafficLightRoadSpacing
                 )
            );

            // PEDESTRIANS
            add( TrafficLight.TYPE_PEDESTRIANS,
                 TrafficLight.ORIENTATION_EAST,
                 TrafficLight.STATE_OFF,
                 new Position(
                     centerX + ( RoadComponent.WIDTH / 2 ) + trafficLightRoadSpacing,
                     centerY - ( RoadComponent.WIDTH / 2 ) - RoadComponent.BERM_WIDTH - TrafficLightPedestriansComponent.WIDTH - trafficLightRoadSpacing
                 )
            );
            add( TrafficLight.TYPE_PEDESTRIANS,
                 TrafficLight.ORIENTATION_EAST,
                 TrafficLight.STATE_OFF,
                 new Position(
                     centerX + ( RoadComponent.WIDTH / 2 ) + trafficLightRoadSpacing,
                     centerY + ( RoadComponent.WIDTH / 2 ) + RoadComponent.BERM_WIDTH + SidewalkComponent.WIDTH - TrafficLightPedestriansComponent.WIDTH - trafficLightRoadSpacing
                 )
            );
            add( TrafficLight.TYPE_PEDESTRIANS,
                 TrafficLight.ORIENTATION_SOUTH,
                 TrafficLight.STATE_OFF,
                 new Position(
                     centerX + ( RoadComponent.WIDTH / 2 ) + RoadComponent.BERM_WIDTH + trafficLightRoadSpacing,
                     centerY + ( RoadComponent.WIDTH / 2 ) + trafficLightRoadSpacing
                 )
            );
            add( TrafficLight.TYPE_PEDESTRIANS,
                 TrafficLight.ORIENTATION_SOUTH,
                 TrafficLight.STATE_OFF,
                 new Position(
                     centerX - ( RoadComponent.WIDTH / 2 ) - RoadComponent.BERM_WIDTH - SidewalkComponent.WIDTH + trafficLightRoadSpacing,
                     centerY + ( RoadComponent.WIDTH / 2 ) + trafficLightRoadSpacing
                 )
            );
            add( TrafficLight.TYPE_PEDESTRIANS,
                 TrafficLight.ORIENTATION_WEST,
                 TrafficLight.STATE_OFF,
                 new Position(
                     centerX - ( RoadComponent.WIDTH / 2 ) - RoadComponent.BERM_WIDTH - trafficLightRoadSpacing,
                     centerY + ( RoadComponent.WIDTH / 2 ) + RoadComponent.BERM_WIDTH + trafficLightRoadSpacing
                 )
            );
            add( TrafficLight.TYPE_PEDESTRIANS,
                 TrafficLight.ORIENTATION_WEST,
                 TrafficLight.STATE_OFF,
                 new Position(
                     centerX - ( RoadComponent.WIDTH / 2 ) - RoadComponent.BERM_WIDTH - trafficLightRoadSpacing,
                     centerY - ( RoadComponent.WIDTH / 2 ) - RoadComponent.BERM_WIDTH - SidewalkComponent.WIDTH + trafficLightRoadSpacing
                 )
            );
            add( TrafficLight.TYPE_PEDESTRIANS,
                 TrafficLight.ORIENTATION_NORTH,
                 TrafficLight.STATE_OFF,
                 new Position(
                     centerX + ( RoadComponent.WIDTH / 2 ) + RoadComponent.BERM_WIDTH + SidewalkComponent.WIDTH - TrafficLightPedestriansComponent.WIDTH - trafficLightRoadSpacing,
                     centerY - ( RoadComponent.WIDTH / 2 ) - RoadComponent.BERM_WIDTH - trafficLightRoadSpacing
                 )
            );
            add( TrafficLight.TYPE_PEDESTRIANS,
                 TrafficLight.ORIENTATION_NORTH,
                 TrafficLight.STATE_OFF,
                 new Position(
                     centerX - ( RoadComponent.WIDTH / 2 ) - RoadComponent.BERM_WIDTH - TrafficLightPedestriansComponent.WIDTH - trafficLightRoadSpacing,
                     centerY - ( RoadComponent.WIDTH / 2 ) - RoadComponent.BERM_WIDTH - trafficLightRoadSpacing
                 )
            );
        }};

        return getBuilder();
    }

}