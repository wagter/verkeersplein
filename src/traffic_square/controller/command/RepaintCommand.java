package traffic_square.controller.command;

import traffic_square.component.RepaintAble;

import java.util.List;

/**
 * A command to repaint a list of JComponent instances
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class RepaintCommand implements Command
{
    /**
     * The list of JComponent instances
     */
    private List<RepaintAble> repaintAbles;

    /**
     * RepaintCommand constructor
     *
     * @param repaintAbles the list of JComponent instances
     */
    public RepaintCommand( List<RepaintAble> repaintAbles )
    {
        this.repaintAbles = repaintAbles;
    }

    @Override
    public void perform()
    {
        for ( RepaintAble repaintAble : repaintAbles ) {
            repaintAble.repaint();
        }
    }

    @Override
    public String getMessage()
    {
        return null;
    }
}
