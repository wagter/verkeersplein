package traffic_square.controller.command;

import traffic_square.traffic_light.TrafficLight;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * A command to switch a traffic_light instance, matched by type and orientation, to a different state
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class SwitchCommand implements Command
{
    /**
     * The list of traffic_light instances in which to search ones to switch
     */
    private List<TrafficLight> lights;

    /**
     * The type of the traffic_light instance to switch
     */
    private String lightType;

    /**
     * The orientation of the traffic_light instance to switch
     */
    private String lightOrientation;

    /**
     * The state to switch the traffic_light instance to
     */
    private String lightState;

    /**
     * SwitchCommand constructor
     *
     * @param lights the list of traffic_light instances in which to search ones to switch
     * @param lightType the type of the traffic_light instance to switch
     * @param lightOrientation the orientation of the traffic_light instance to switch
     * @param lightState the state to switch the traffic_light instance to
     */
    public SwitchCommand(
        @NotNull List<TrafficLight> lights,
        @NotNull String lightType,
        @NotNull String lightOrientation,
        @NotNull String lightState
    )
    {
        this.lights = lights;
        this.lightType = lightType;
        this.lightOrientation = lightOrientation;
        this.lightState = lightState;
    }

    @Override
    public void perform()
    {
        for ( TrafficLight light : lights ) {
            if ( matches( light ) ) {
                light.setState( lightState );
            }
        }
    }


    @Override
    public String getMessage()
    {
        return (char) 183 + " Switch " + lightType + " " + lightOrientation + " to " + lightState;
    }

    /**
     * Check if the given traffic_light instance matches this command
     *
     * @param light the traffic_light instance to check for match
     * @return true if the traffic_light instance matches this command
     */
    private boolean matches( @NotNull TrafficLight light )
    {
        return light.isType( lightType )
            && light.isOrientation( lightOrientation )
            && !light.isState( lightState );
    }
}
