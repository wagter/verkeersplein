package traffic_square.controller.command;

import java.util.Queue;

/**
 * A runnable queue to enqueue and perform commands
 */
public interface CommandQueue extends Queue<Command>, Runnable
{
    /**
     * Check if the queue is running
     *
     * @return true if the queue is running
     */
    boolean isRunning();

    /**
     * Start the queue
     *
     * @author Joris Wagter [jrswgtr@gmail.com]
     */
    void start();
}
