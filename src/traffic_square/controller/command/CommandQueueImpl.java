package traffic_square.controller.command;

import java.util.*;

/**
 * CommandQueue implementation
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class CommandQueueImpl extends LinkedList<Command> implements CommandQueue
{
    /**
     * The CommandMessageList instance to insert the command messages
     */
    private CommandMessageList messageList;

    /**
     * True if the queue is currently running
     */
    private boolean running;

    /**
     * CommandQueueImpl constructor
     *
     * @param messageList a CommandMessageList instance containing the messages
     */
    public CommandQueueImpl( CommandMessageList messageList )
    {
        this.messageList = messageList;
        this.running = false;
    }

    @Override
    public boolean add( Command com )
    {
        messageList.add( com );
        return super.add( com );
    }

    @Override
    public void clear()
    {
        messageList.clear();
        super.clear();
    }

    @Override
    public boolean isRunning()
    {
        return running;
    }

    @Override
    public void start()
    {
        if ( !isRunning() ) {
            setRunning( true );
            Thread thread = new Thread( this );
            thread.start();
        }
    }

    @Override
    public void run()
    {
        while ( peek() != null ) {
            Command com = poll();
            com.perform();
            messageList.remove( com );
        }
        setRunning( false );
    }

    /**
     * Set the running state
     *
     * @param running if the queue is running
     */
    private void setRunning( boolean running )
    {
        this.running = running;
    }
}
