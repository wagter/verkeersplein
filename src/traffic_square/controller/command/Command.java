package traffic_square.controller.command;

/**
 * A command to perform by a CommandQueue instance
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface Command
{
    /**
     * Perform the command
     */
    void perform();

    /**
     * Get the command message
     *
     * @return the command message
     */
    String getMessage();
}
