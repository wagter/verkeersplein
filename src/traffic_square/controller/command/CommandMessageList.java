package traffic_square.controller.command;

import java.util.List;

/**
 * A list to keep messages from enqueued command instances
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface CommandMessageList extends List<String>
{
    /**
     * Add a command message to the list
     *
     * @param com the command to get the message from
     */
    void add( Command com );

    /**
     * Add a command message to the list
     *
     * @param com the command to get the message from
     */
    void remove( Command com );

    /**
     * Update the command message list
     */
    void update();

    /**
     * Reset the command message list
     */
    void reset();
}
