package traffic_square.controller.command;

import org.jetbrains.annotations.NotNull;

import java.util.*;

/**
 * Implementing the CommandMessageList interface
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class CommandMessageListImpl extends ArrayList<String> implements CommandMessageList
{
    /**
     * The messages, mapped by their order
     */
    private Map<Integer, String> messages;

    /**
     * The message positions, mapped by their command's hash code
     */
    private Map<Integer, Integer> commandMessageMap;

    /**
     * The message counter to maintain the right sort order
     */
    private int messageCounter = 0;

    /**
     * CommandMessageListImpl constructor
     */
    public CommandMessageListImpl()
    {
        reset();
    }

    @Override
    public void add( Command com )
    {
        if ( com.getMessage() != null ) {
            commandMessageMap.put( com.hashCode(), messageCounter );
            messages.put( messageCounter, com.getMessage() );
            messageCounter++;
        }
    }

    @Override
    public void remove( Command com )
    {
        Integer hashCode = com.hashCode();
        if ( !commandMessageMap.containsKey( hashCode ) ) {
            return;
        }

        Integer index = commandMessageMap.get( hashCode );
        if ( !messages.containsKey( index ) ) {
            return;
        }

        messages.remove( index );
        commandMessageMap.remove( hashCode );
    }

    @Override
    public void update()
    {
        clear();

        TreeMap<Integer, String> sorted = new TreeMap<>( messages );

        for ( Map.Entry<Integer, String> entry : sorted.entrySet() ) {
            add( entry.getValue() );
        }
    }

    @Override
    @NotNull
    public Iterator<String> iterator()
    {
        update();
        return super.iterator();
    }

    @Override
    public void reset()
    {
        messages = new HashMap<>();
        commandMessageMap = new HashMap<>();
        messageCounter = 0;

        clear();
    }
}
