package traffic_square.controller.command;

import java.util.concurrent.ThreadLocalRandom;

/**
 * A command to trigger a certain sleep time on the CommandQueue instance
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class SleepCommand implements Command
{
    /**
     * The time to sleep in milliseconds
     */
    private int sleepTime;

    /**
     * SleepCommand constructor
     *
     * @param sleepTime the time to sleep in milliseconds
     */
    public SleepCommand( int sleepTime )
    {
        this.sleepTime = sleepTime;
    }

    @Override
    public void perform()
    {
        try {
            Thread.sleep( sleepTime );
        } catch ( InterruptedException e ) {
            e.printStackTrace();
        }
    }

    @Override
    public String getMessage()
    {
        return "\n" + (char)183 + " Wait for " + sleepTime + " milliseconds\n";
    }

    /**
     * Generate a random sleep time in a certain range
     *
     * @param min the minimum sleep time in milliseconds
     * @param max the maximum sleep time in milliseconds
     * @return the randomly generated sleep time
     */
    public static int randomSleepTime( int min, int max )
    {
        return ThreadLocalRandom.current().nextInt( min, max + 1);
    }
}
