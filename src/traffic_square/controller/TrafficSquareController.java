package traffic_square.controller;

import traffic_square.controller.command.*;
import traffic_square.component.RepaintAble;
import traffic_square.traffic_light.TrafficLight;
import traffic_square.controller.command.CommandQueue;

import java.util.*;

/**
 * controller to control the traffic traffic_square program
 */
public class TrafficSquareController
{
    /**
     * Queue to enqueue new commands
     */
    private CommandQueue commandQueue;

    /**
     * List of elements to repaint
     */
    private List<RepaintAble> repaintAbles;

    /**
     * If the program is in "on" state
     */
    private boolean on = false;

    /**
     * The current step in the program
     */
    private int currentStep = 0;

    /**
     * The available steps in the program
     */
    private List<Step> steps;

    /**
     * The list of traffic_light instances to control
     */
    private List<TrafficLight> lights;

    /**
     * TrafficSquareController constructor
     *
     * @param commandQueue a queue to enqueue new commands
     * @param lights a list of traffic_light instances to control
     * @param repaintAbles a list of elements to repaint
     */
    public TrafficSquareController(
        CommandQueue commandQueue,
        List<TrafficLight> lights,
        List<RepaintAble> repaintAbles
    )
    {
        this.commandQueue = commandQueue;
        this.repaintAbles = repaintAbles;
        this.lights = lights;
    }

    /**
     * Toggle the state of the application to on or off
     */
    public void toggleOnAction()
    {
        if ( on ) {
            commandQueue.clear();
            repaintAction();
            offAction();
        } else {
            onAction();
        }
    }

    /**
     * Perform all steps in a row
     */
    public void loopAction()
    {
        if ( !on ) {
            return;
        }

        for ( int i = currentStep; i < getSteps().size(); i++ ) {
            getSteps().get( i ).step();
        }

        currentStep = 0;
    }

    /**
     * Perform the next step
     */
    public void stepAction()
    {
        getSteps().get( currentStep ).step();
        currentStep++;
        if ( currentStep >= getSteps().size() ) {
            currentStep = 0;
        }
    }

    /**
     * Set all traffic_light instances to red state
     */
    private void onAction()
    {
        on = true;

        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_NORTH, TrafficLight.STATE_RED ) );
        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_SOUTH, TrafficLight.STATE_RED ) );
        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_EAST, TrafficLight.STATE_RED ) );
        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_WEST, TrafficLight.STATE_RED ) );
        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_VEHICLES, TrafficLight.ORIENTATION_NORTH, TrafficLight.STATE_RED ) );
        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_VEHICLES, TrafficLight.ORIENTATION_SOUTH, TrafficLight.STATE_RED ) );
        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_VEHICLES, TrafficLight.ORIENTATION_EAST, TrafficLight.STATE_RED ) );
        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_VEHICLES, TrafficLight.ORIENTATION_WEST, TrafficLight.STATE_RED ) );

        commandQueue.add( new RepaintCommand( repaintAbles ) );

        repaintAction();
        commandQueue.start();
    }

    /**
     * Set all traffic_light instances to STATE_OFF
     */
    private void offAction()
    {
        on = false;

        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_NORTH, TrafficLight.STATE_OFF ) );
        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_SOUTH, TrafficLight.STATE_OFF ) );
        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_EAST, TrafficLight.STATE_OFF ) );
        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_WEST, TrafficLight.STATE_OFF ) );
        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_VEHICLES, TrafficLight.ORIENTATION_NORTH, TrafficLight.STATE_OFF ) );
        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_VEHICLES, TrafficLight.ORIENTATION_SOUTH, TrafficLight.STATE_OFF ) );
        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_VEHICLES, TrafficLight.ORIENTATION_NORTH, TrafficLight.STATE_OFF ) );
        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_VEHICLES, TrafficLight.ORIENTATION_SOUTH, TrafficLight.STATE_OFF ) );
        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_VEHICLES, TrafficLight.ORIENTATION_EAST, TrafficLight.STATE_OFF ) );
        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_VEHICLES, TrafficLight.ORIENTATION_WEST, TrafficLight.STATE_OFF ) );
        commandQueue.add( new RepaintCommand( repaintAbles ) );

        repaintAction();
        commandQueue.start();
    }

    /**
     * Set all traffic_light instances with type VEHICLE of a given orientation to STATE_GREEN
     *
     * @param orientation the orientation to set to STATE_GREEN
     */
    private void vehiclesGoAction( String orientation )
    {
        if ( !on ) {
            return;
        }

        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_VEHICLES, orientation, TrafficLight.STATE_GREEN ) );
        commandQueue.add( new RepaintCommand( repaintAbles ) );

        repaintAction();
        commandQueue.start();
    }

    /**
     * Set all traffic_light instances with type VEHICLE of a given orientation to STATE_ORANGE, then STATE_RED
     *
     * @param orientation the orientation to set to STATE_RED
     */
    private void vehiclesStopAction( String orientation )
    {
        if ( !on ) {
            return;
        }

        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_VEHICLES, orientation, TrafficLight.STATE_YELLOW ) );
        commandQueue.add( new RepaintCommand( repaintAbles ) );

        commandQueue.add( new SleepCommand( 3500 ) );

        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_VEHICLES, orientation, TrafficLight.STATE_RED ) );
        commandQueue.add( new RepaintCommand( repaintAbles ) );

        repaintAction();
        commandQueue.start();
    }

    /**
     * Set all traffic_light instances with of TYPE_PEDESTRIAN to STATE_GREEN
     */
    private void pedestriansGoAction()
    {
        if ( !on ) {
            return;
        }

        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_NORTH, TrafficLight.STATE_GREEN ) );
        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_SOUTH, TrafficLight.STATE_GREEN ) );
        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_EAST, TrafficLight.STATE_GREEN ) );
        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_WEST, TrafficLight.STATE_GREEN ) );

        commandQueue.add( new RepaintCommand( repaintAbles ) );

        repaintAction();
        commandQueue.start();
    }

    /**
     * Set all traffic_light instances with of TYPE_PEDESTRIAN to STATE_RED
     */
    private void pedestriansStopAction()
    {
        if ( !on ) {
            return;
        }

        for ( int i = 0; i < 5; i++ ) {
            commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_NORTH, TrafficLight.STATE_OFF ) );
            commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_SOUTH, TrafficLight.STATE_OFF ) );
            commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_EAST, TrafficLight.STATE_OFF ) );
            commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_WEST, TrafficLight.STATE_OFF ) );
            commandQueue.add( new RepaintCommand( repaintAbles ) );

            commandQueue.add( new SleepCommand( 500 ) );

            commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_NORTH, TrafficLight.STATE_GREEN ) );
            commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_SOUTH, TrafficLight.STATE_GREEN ) );
            commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_EAST, TrafficLight.STATE_GREEN ) );
            commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_WEST, TrafficLight.STATE_GREEN ) );
            commandQueue.add( new RepaintCommand( repaintAbles ) );

            commandQueue.add( new SleepCommand( 500 ) );
        }

        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_NORTH, TrafficLight.STATE_OFF ) );
        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_SOUTH, TrafficLight.STATE_OFF ) );
        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_EAST, TrafficLight.STATE_OFF ) );
        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_WEST, TrafficLight.STATE_OFF ) );
        commandQueue.add( new RepaintCommand( repaintAbles ) );

        commandQueue.add( new SleepCommand( 500 ) );

        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_NORTH, TrafficLight.STATE_RED ) );
        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_SOUTH, TrafficLight.STATE_RED ) );
        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_EAST, TrafficLight.STATE_RED ) );
        commandQueue.add( new SwitchCommand( lights, TrafficLight.TYPE_PEDESTRIANS, TrafficLight.ORIENTATION_WEST, TrafficLight.STATE_RED ) );
        commandQueue.add( new RepaintCommand( repaintAbles ) );

        repaintAction();
        commandQueue.start();
    }

    /**
     * Sleep for a given time in milliseconds
     *
     * @param millis a given time in milliseconds
     */
    private void sleepAction( int millis )
    {
        if ( !on ) {
            return;
        }

        commandQueue.add( new SleepCommand( millis ) );

        repaintAction();
        commandQueue.start();
    }

    /**
     * Sleep for a random time between 10000 and 20000 in milliseconds
     */
    private void sleepAction()
    {
        sleepAction( SleepCommand.randomSleepTime( 10000, 20000 ) );
    }

    /**
     * Update the message queue
     */
    private void repaintAction()
    {
        new RepaintCommand( repaintAbles ).perform();
    }

    /**
     * Returns the list with steps to perform in the right order
     *
     * @return the list of steps
     */
    private List<Step> getSteps()
    {
        if ( steps != null ) {
            return steps;
        }

        steps = new ArrayList<Step>()
        {{
            add( () -> {
                vehiclesGoAction( TrafficLight.ORIENTATION_NORTH );
                sleepAction();
            } );
            add( () -> {
                vehiclesStopAction( TrafficLight.ORIENTATION_NORTH );
                sleepAction( 3000 );
                vehiclesGoAction( TrafficLight.ORIENTATION_EAST );
                sleepAction();
            } );
            add( () -> {
                vehiclesStopAction( TrafficLight.ORIENTATION_EAST );
                sleepAction( 3000 );
                pedestriansGoAction();
                sleepAction();
            } );
            add( () -> {
                pedestriansStopAction();
                sleepAction( 3000 );
                vehiclesGoAction( TrafficLight.ORIENTATION_SOUTH );
                sleepAction();
            } );
            add( () -> {
                vehiclesStopAction( TrafficLight.ORIENTATION_SOUTH );
                sleepAction( 3000 );
                vehiclesGoAction( TrafficLight.ORIENTATION_WEST );
                sleepAction();
            } );
            add( () -> {
                vehiclesStopAction( TrafficLight.ORIENTATION_WEST );
                sleepAction( 3000 );
                pedestriansGoAction();
                sleepAction();
            } );
            add( () -> {
                pedestriansStopAction();
                sleepAction( 3000 );
            } );
        }};

        return getSteps();
    }
}
