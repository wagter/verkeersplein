package traffic_square.controller;

/**
 * A functional interface to provide executable steps with Lambda expressions for a TrafficSquareController instance.
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
interface Step
{
    /**
     * Do the step
     */
    void step();
}
