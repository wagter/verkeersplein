package traffic_square.traffic_light;

/**
 * Interface representing a traffic traffic_square
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface TrafficLight
{
    String TYPE_VEHICLES = "vehicles";
    String TYPE_PEDESTRIANS = "pedestrians";

    String STATE_OFF = "off";
    String STATE_GREEN = "green";
    String STATE_YELLOW = "yellow";
    String STATE_RED = "red";

    String ORIENTATION_NORTH = "north";
    String ORIENTATION_EAST = "east";
    String ORIENTATION_SOUTH = "south";
    String ORIENTATION_WEST = "west";

    /**
     * Get the type of the traffic traffic_square
     *
     * @return the type of the traffic traffic_square
     */
    String getType();

    /**
     * Set the type of the traffic traffic_square
     *
     * @param type the type of the traffic traffic_square
     */
    void setType( String type );

    /**
     * Check if the type of the traffic traffic_square is the same as the argument.
     *
     * @param type the type to check the traffic traffic_square for
     * @return true if the type of the traffic traffic_square is the same as the argument.
     */
    boolean isType( String type );

    /**
     * Get the state of the traffic traffic_square
     *
     * @return the state of the traffic traffic_square
     */
    String getState();

    /**
     * Set the state of the traffic traffic_square
     *
     * @param state the state of the traffic traffic_square
     */
    void setState( String state );

    /**
     * Check if the state of the traffic traffic_square is the same as the argument.
     *
     * @param state the type to check the traffic traffic_square for
     * @return true if the state of the traffic traffic_square is the same as the argument.
     */
    boolean isState( String state );

    /**
     * Get the orientation of the traffic traffic_square
     *
     * @return the orientation of the traffic traffic_square
     */
    String getOrientation();

    /**
     * Set the orientation of the traffic traffic_square
     *
     * @param orientation the orientation of the traffic traffic_square
     */
    void setOrientation( String orientation );

    /**
     * Check if the orientation of the traffic traffic_square is the same as the argument.
     *
     * @param orientation the type to check the traffic traffic_square for
     * @return true if the orientation of the traffic traffic_square is the same as the argument.
     */
    boolean isOrientation( String orientation );
}
