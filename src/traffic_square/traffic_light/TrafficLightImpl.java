package traffic_square.traffic_light;

/**
 * traffic_light implementation
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class TrafficLightImpl implements TrafficLight
{
    private String type, orientation, state;

    /**
     * TrafficLightImpl constructor
     *
     * @param type the type of the traffic traffic_square
     * @param orientation the orientation of the traffic traffic_square
     * @param state the state of the traffic traffic_square
     */
    public TrafficLightImpl( String type, String orientation, String state )
    {
        this.type = type;
        this.orientation = orientation;
        this.state = state;
    }

    @Override
    public String getOrientation()
    {
        return orientation;
    }

    @Override
    public void setOrientation( String orientation )
    {
        this.orientation = orientation;
    }

    @Override
    public boolean isOrientation( String orientation )
    {
        return this.orientation.equals( orientation );
    }

    @Override
    public String getType()
    {
        return type;
    }

    @Override
    public void setType( String type )
    {
        this.type = type;
    }

    @Override
    public boolean isType( String type )
    {
        return this.type.equals( type );
    }

    @Override
    public String getState()
    {
        return state;
    }

    @Override
    public void setState( String state )
    {
        this.state = state;
    }

    @Override
    public boolean isState( String state )
    {
        return this.state.equals( state );
    }
}
