package traffic_square.traffic_light;

/**
 * Factory to instantiate traffic_light objects
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface TrafficLightFactory
{
    /**
     * Create a new traffic_light object
     *
     * @param type the type of the traffic_light
     * @param orientation the orientation of the traffic_light
     * @param state the state of the traffic_light
     *
     * @return the newly instantiated traffic_light object
     */
    TrafficLight create( String type, String orientation, String state );
}
