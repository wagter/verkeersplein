package traffic_square.traffic_light;

/**
 * TrafficLightFactory implementation
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class TrafficLightFactoryImpl implements TrafficLightFactory
{
    @Override
    public TrafficLight create( String type, String orientation, String state )
    {
        return new TrafficLightImpl( type, orientation, state );
    }
}
