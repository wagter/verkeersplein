package traffic_square.listener;

import traffic_square.controller.TrafficSquareController;

import java.awt.event.ActionEvent;

/**
 * listener to trigger a "next step" action on the TrafficSquareController
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class StepActionListener extends TrafficSquareActionListener
{
    /**
     * StepActionListener constructor
     *
     * @param controller the TrafficSquareController instance to trigger "next step" action on
     */
    public StepActionListener( TrafficSquareController controller )
    {
        super( controller );
    }

    @Override
    public void actionPerformed( ActionEvent e )
    {
        controller.stepAction();
    }
}
