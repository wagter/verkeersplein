package traffic_square.listener;

import traffic_square.controller.TrafficSquareController;

import java.awt.event.ActionEvent;

/**
 * listener to trigger a "toggle on" action on the TrafficSquareController
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class ToggleActionListener extends TrafficSquareActionListener
{
    /**
     * ToggleActionListener constructor
     *
     * @param controller the TrafficSquareController instance to trigger "toggle on" action on
     */
    public ToggleActionListener( TrafficSquareController controller )
    {
        super( controller );
    }

    @Override
    public void actionPerformed( ActionEvent e )
    {
        controller.toggleOnAction();
    }
}
