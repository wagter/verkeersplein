package traffic_square.listener;

import traffic_square.controller.TrafficSquareController;

import java.awt.event.ActionEvent;

/**
 * listener to trigger a loop action on the TrafficSquareController
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class LoopActionListener extends TrafficSquareActionListener
{
    /**
     * LoopActionListener constructor
     *
     * @param controller the TrafficSquareController instance to trigger the loop action on
     */
    public LoopActionListener( TrafficSquareController controller )
    {
        super( controller );
    }

    @Override
    public void actionPerformed( ActionEvent e )
    {
        controller.loopAction();
    }
}
