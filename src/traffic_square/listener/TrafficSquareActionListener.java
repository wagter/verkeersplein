package traffic_square.listener;

import traffic_square.controller.TrafficSquareController;

import java.awt.event.ActionListener;

public abstract class TrafficSquareActionListener implements ActionListener
{
    protected TrafficSquareController controller;

    public TrafficSquareActionListener( TrafficSquareController controller )
    {
        this.controller = controller;
    }
}
