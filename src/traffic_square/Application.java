package traffic_square;

import traffic_square.config.exception.InvalidConfigException;

/**
 * The main interface for the Application application
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface Application
{
    /**
     * Initialize the application
     *
     * @throws InvalidConfigException if needed config is not present
     */
    void init() throws InvalidConfigException;
}
