package traffic_square.builder;

import traffic_square.component.TrafficLightComponent;
import traffic_square.component.TrafficLightComponentFactory;
import traffic_square.traffic_light.TrafficLight;
import traffic_square.traffic_light.TrafficLightFactory;
import traffic_square.util.Position;

import java.util.ArrayList;
import java.util.List;

/**
 * TrafficLightBuilder implementation
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class TrafficLightBuilderImpl implements TrafficLightBuilder
{
    private List<TrafficLight> lights;

    private List<TrafficLightComponent> components;

    private TrafficLightFactory lightFactory;

    private TrafficLightComponentFactory componentFactory;

    public TrafficLightBuilderImpl( TrafficLightFactory lightFactory, TrafficLightComponentFactory componentFactory )
    {
        lights = new ArrayList<>();
        components = new ArrayList<>();

        this.lightFactory = lightFactory;
        this.componentFactory = componentFactory;
    }

    @Override
    public void add( String type, String orientation, String state, Position componentPosition )
    {
        TrafficLight light = lightFactory.create( type, orientation, state );
        TrafficLightComponent component = componentFactory.create( light, componentPosition );

        lights.add( light );
        components.add( component );
    }

    @Override
    public List<TrafficLight> getLights()
    {
        return lights;
    }

    @Override
    public List<TrafficLightComponent> getComponents()
    {
        return components;
    }
}
