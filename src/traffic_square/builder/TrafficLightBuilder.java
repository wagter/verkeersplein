package traffic_square.builder;

import traffic_square.component.TrafficLightComponent;
import traffic_square.traffic_light.TrafficLight;
import traffic_square.util.Position;

import java.util.List;

/**
 * builder class to build traffic_light and TrafficLightComponent instances
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public interface TrafficLightBuilder
{
    /**
     * Add a new traffic_light to the builder
     *
     * @param type the type of the traffic traffic_square
     * @param orientation the orientation of the traffic traffic_square
     * @param state the state of the traffic traffic_square
     * @param componentPosition the position of the component
     */
    void add( String type, String orientation, String state,  Position componentPosition );

    /**
     * Get the built traffic_light instances
     *
     * @return the list of traffic_light instances
     */
    List<TrafficLight> getLights();

    /**
     * Get the built TrafficLightComponent instances
     *
     * @return the list of TrafficLightComponent instances
     */
    List<TrafficLightComponent> getComponents();
}
