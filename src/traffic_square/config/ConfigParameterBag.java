package traffic_square.config;

import traffic_square.config.exception.InvalidConfigException;

import java.awt.*;
import java.util.Map;

/**
 * Parameter bag to contain config parameters
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class ConfigParameterBag
{
    private Map<String, Integer> integers;

    private Map<String, Color> colors;

    /**
     * ConfigParameterBag constructor
     *
     * @param integers a map of String, Integer pairs
     * @param colors a list of String, Color pairs
     */
    public ConfigParameterBag( Map<String, Integer> integers, Map<String, Color> colors )
    {
        this.integers = integers;
        this.colors = colors;
    }

    /**
     * Get an Integer by its key
     *
     * @param key the key of the Integer value
     * @return the Integer value
     * @throws InvalidConfigException if the requested key is not present in the Integer map
     */
    public Integer getInteger( String key ) throws InvalidConfigException
    {
        if ( !integers.containsKey( key ) ) {
            throw new InvalidConfigException( key );
        }

        return integers.get( key );
    }

    /**
     * Get a Color by its key
     *
     * @param key the key of the Color value
     * @return the Color value
     * @throws InvalidConfigException if the requested key is not present in the Color map
     */
    public Color getColor( String key ) throws InvalidConfigException
    {
        if ( !colors.containsKey( key ) ) {
            throw new InvalidConfigException( key );
        }

        return colors.get( key );
    }
}
