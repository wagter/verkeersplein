package traffic_square.config.exception;

/**
 * exception to throw when needed config is not present in the ConfigParameterBag
 *
 * @author Joris Wagter [jrswgtr@gmail.com]
 */
public class InvalidConfigException extends Exception
{
    private String key;

    /**
     * InvalidConfigException constructor
     *
     * @param key the key that should be present
     */
    public InvalidConfigException( String key )
    {
        this.key = key;
    }

    @Override
    public String getMessage()
    {
        return "Parameter \"" + key + "\" is not present in current ConfigParameterBag object!";
    }
}
