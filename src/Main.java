import traffic_square.*;
import traffic_square.config.ConfigParameterBag;
import traffic_square.config.exception.InvalidConfigException;

import java.awt.*;
import java.util.HashMap;

/**
 * Startup class for the TrafficSquare application
 *
 * @author Joris Wagter
 */
public class Main
{
    public static void main( String... args )
    {
        try {
            Application application = new TrafficSquare( getConfig() );
            application.init();
        } catch ( InvalidConfigException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Get the configuration parameters for the application
     *
     * @return the configuration parameters for the application
     */
    private static ConfigParameterBag getConfig()
    {
        return new ConfigParameterBag(
            new HashMap<String, Integer>()
            {{
                put( "background_width", 1024 );
                put( "background_height", 768 );
                put( "control_panel_width", 300 );
            }},
            new HashMap<String, Color>()
            {{
                put( "background", new Color( 33, 33, 33 ) );
                put( "text", new Color( 255, 255, 255 ) );
            }}
        );
    }
}
